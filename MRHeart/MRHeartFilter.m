//
//  MRHeartFilter.m
//  MRHeart
//
//  Copyright (c) 2014 Konrad Werys. All rights reserved.
//

/** TODO:
 1. more roi tools, not only tcpolygon
 2. move roi names and colores from myDrawROI
 3. how to check if image is changed?
 4. bsa calculation on given weight and height, not just automatic
 5. interpolation of rois between end systole and end diastole
 6. show which frames have contours (in table?)
 7. import/export of contures to/from qmass
 8. make it work on philips and GE cardiac images (depending on scaner switch number of slices with nuber of images)
 9. nice tutorial
 */
 

#import "MRHeartFilter.h"

@implementation MRHeartFilter

- (void) initPlugin
{
}

- (long) filterImage:(NSString*) menuName
{
    // Window Controller
    NSWindowController *window = [[NSWindowController alloc] initWithWindowNibName:@"MRHeartPanel" owner:self];
    [window showWindow:self];

//    How to make keyboard work
//    NSLog(@"WC first responder %d",[window acceptsFirstResponder]);
//    NSLog(@"VC first responder %d",[viewerController acceptsFirstResponder]);
//    NSLog(@"mrW %d",[mrWindow acceptsFirstResponder]);

    // calc BSA and dispalay
    NSString *BSAstring = [NSString stringWithFormat:@"%.2f", [self calcBSA]];
    [BSAfield setStringValue: BSAstring];
    // get normal vectors, 
    [LVparams setStringValue:@"LV:\n0\n0\n0\n0\n0\n"];
    [LVBSAparams setStringValue:@"NLV:\n0\n0\n0\n0\n0\n"];
    [RVparams setStringValue:@"RV:\n0\n0\n0\n0\n0\n"];
    [RVBSAparams setStringValue:@"NRV:\n0\n0\n0\n0\n0\n"];
    
    // set ES and ED
    [self autoSetESED:0];
    
    return 0; // No Errors
    
//    NSLog(@"Selected slices:%hd",[viewerController maxMovieIndex]);
//    for (int j = 0; j < [viewerController maxMovieIndex]; j++)
//    {
//        NSArray     *PixList = [viewerController pixList: j];
//        DCMPix      *curPix = [PixList objectAtIndex: 0];
//        NSLog(@"Slice nr %d. Frames: %d",j,[PixList count]);
//        
//        NSMutableString    *myorientationString = [[NSMutableString alloc] init]; // tylko do wyswietlania
//        double       myorientation[9];
//        [curPix orientationDouble: myorientation];
//        
//        // wyswietl origin
//        NSLog(@"Origin: %g %g %g",[curPix originX],[curPix originY],[curPix originZ]);
//        for (int k = 0; k < 9; k++)
//        {
//            [myorientationString appendFormat: @"%g ", myorientation[k]];
//        }
//        // wyswietl orientation
//        NSLog(@"Orientation: %@",myorientationString);
//    }
    
}

/*
 plugin calculation functions
 */

// sets end systolic and end diastolic based on volume of LVENDO. If no volumes in LVENDO, sets ED=1, ES=ntimes/2
-(IBAction)autoSetESED:(id)sender;
{
    //if volumes are set
    NSMutableArray *volumesArrayLVENDO = [self calcVolumesNew:@"LVENDO"];

    NSNumber *mymax = [NSNumber numberWithDouble:-MAXFLOAT];
    NSNumber *mymin = [NSNumber numberWithDouble:MAXFLOAT];
    int mymaxIDX = 0;
    int myminIDX = 0;
    for (int i = 0; i < [volumesArrayLVENDO count]; i++)
    {
        NSNumber *temp = [volumesArrayLVENDO objectAtIndex:i];
        if (([temp doubleValue] < [mymin doubleValue]) && ([temp doubleValue]!=0)) {
            mymin = temp;
            myminIDX = i;
        }
        if ([temp doubleValue] > [mymax doubleValue]){
            mymax = temp;
            mymaxIDX = i;
        }
    }
    
    NSLog(@"Min:%@ %i Max:%@ %i",mymin,myminIDX,mymax,mymaxIDX);
    
    if (([mymin doubleValue]!=0) && ([mymax doubleValue]!=0)){
        ED.intValue = mymaxIDX  + 1;
        ES.intValue = myminIDX + 1;
    } else{
        ED.intValue = 1;
        ES.intValue = [[viewerController pixList] count]/2+1;
    }
}

// from http://en.wikipedia.org/wiki/Body_surface_area
-(float) calcBSA
{
    float BSA = 0;
    
    @try{
        // get first image
        NSArray     *PixList = [viewerController pixList: 0];
        DCMPix      *curPix = [PixList objectAtIndex: 0];
        // get dicom object
        DCMObject   *dcmObj = [DCMObject objectWithContentsOfFile:[curPix sourceFile] decodingPixelData:NO];
        DCMAttributeTag *patientsWeightTag = [DCMAttributeTag tagWithName:@"PatientsWeight"];
        DCMAttributeTag *patientsSizeTag = [DCMAttributeTag tagWithName:@"PatientsSize"];
        // get data from dicom tags
        float patientsWeight = [[[[dcmObj attributeForTag:patientsWeightTag] value] description] doubleValue];
        float patientsSize   = [[[[dcmObj attributeForTag:patientsSizeTag] value] description] doubleValue];
        BSA=sqrt(patientsWeight*patientsSize)/6;
        
    } @catch (NSException *exception){
        NSLog(@"BSA calculation exception: %@",exception);
    }
    return BSA;
}

// do not check if parallel, this should be checked before
// in fact we are looking for distance between point(origin of slice0) and plane (normal vector and origin of slice1)
// distance as in http://en.wikipedia.org/wiki/Plane_(geometry)#Distance_from_a_point_to_a_plane
-(double) calcDistanceBetweenSlices:(int) islice0 and: (int) islice1
{
    double sliceDistance = 0;
    // get PixList for given slices
    NSArray     *PixList0 = [viewerController pixList: islice0];
    NSArray     *PixList1 = [viewerController pixList: islice1];
    
    // get first image objects in selected slice
    DCMPix      *curPix0 = [PixList0 objectAtIndex: 0];
    DCMPix      *curPix1 = [PixList1 objectAtIndex: 0];
    
    // get orientation (to use myorientation1[6-8] as normal vector)
    double      myorientation1[9];
    [curPix1 orientationDouble: myorientation1];
    
    double      sliceOrigins0[3] = {[curPix0 originX],[curPix0 originY],[curPix0 originZ]};
    double      sliceOrigins1[3] = {[curPix1 originX],[curPix1 originY],[curPix1 originZ]};
    // substract orientation point vectors
    double      tempp[3] = {sliceOrigins0[0] - sliceOrigins1[0], sliceOrigins0[1] - sliceOrigins1[1], sliceOrigins0[2] - sliceOrigins1[2]};
    // get normal vector (slice1)
    double      tempnormal1[3] = {myorientation1[6],myorientation1[7],myorientation1[8]};

    sliceDistance = [self dotProduct: tempnormal1 and: tempp];
    //NSLog(@"!!!Distance to previous slice: %g",sliceDistance);
    
    return sliceDistance;
}

/** calculating Volumes
 http://mathworld.wolfram.com/Point-PlaneDistance.html
 http://en.wikipedia.org/wiki/Plane_(geometry)#Distance_from_a_point_to_a_plane
 @TODO: I just found that myorientation[6-8] is vector normal to the plane. First loop can be a little shorter
 @TODO: It would be nice to have the slices sorted before volume calculation
 @TODO: First check if planes are parallel, then allow to use plugin
 @TODO: Using number of frames from first slice assuming that all slices have the same number of frames (this is true for 4d viewer, but it should be corrected)
 */
-(NSMutableArray *)calcVolumesNew:(NSString*) RoiName;
{
    // number of slices selected
    int nslices = [viewerController maxMovieIndex];
    // number of frames in first slice
    NSMutableArray     *PixList = [viewerController pixList: 0];
    int ntimes = [PixList count];
    
    // get areaArray
    NSMutableArray *areaArray;
    areaArray =[self getAreaArrayFor:RoiName];
    
    //get slice thicknesses
    double *sliceThickneses = malloc( nslices * sizeof(double) );
    for (int j = 0; j < nslices; j++)
    {
        NSArray     *PixList = [viewerController pixList: j];
        DCMPix      *curPix = [PixList objectAtIndex: 0];
        sliceThickneses[j]=[curPix sliceThickness];
    }
    
    // do I have to initialize an array like this? =/
    NSNumber *temp =[[NSNumber alloc] initWithDouble:0];
    NSMutableArray *myvolume = [[NSMutableArray alloc] initWithCapacity:ntimes ];
    for (int i = 0; i < ntimes; i++)
        [myvolume insertObject:temp atIndex:i];
    
    // calculate volumes
    for (int itime = 0; itime < ntimes; itime++)
    {
        double tempvolume = 0;
        // calculate how many non empty elements there are
        int slicesWithROIcounter=0;
        for (int islice = 0; islice < nslices; islice++){
            if([areaArray objectAtIndex:islice*ntimes+itime]!=0){
                slicesWithROIcounter++;
            }
        }
        // calculate slice with roi indexes
        int *sliceWithRoiIdx = malloc(slicesWithROIcounter*sizeof(int));
        int i = 0;
        for (int islice = 0; islice < nslices; islice++){
            if([areaArray objectAtIndex:islice*ntimes+itime]!=0){
                sliceWithRoiIdx[i++]=islice;
            }
        }
        
        // calculate volumes
        if (slicesWithROIcounter>0){
            int idxFirst = sliceWithRoiIdx[0];
            tempvolume = sliceThickneses[idxFirst] * [[areaArray objectAtIndex:idxFirst*ntimes+itime] floatValue]/2/10;
            for (int i = 0; i<slicesWithROIcounter-1; i++){
                int idx0 = sliceWithRoiIdx[i];
                int idx1 = sliceWithRoiIdx[i+1];
                double mydistance;
                mydistance = [self calcDistanceBetweenSlices:idx0 and:idx1];
                tempvolume = tempvolume + mydistance * [[areaArray objectAtIndex:idx0*ntimes+itime] floatValue]/2/10;
                tempvolume = tempvolume + mydistance * [[areaArray objectAtIndex:idx1*ntimes+itime] floatValue]/2/10;
            }
            int idxLast = sliceWithRoiIdx[slicesWithROIcounter-1];
            tempvolume = tempvolume + sliceThickneses[idxLast] * [[areaArray objectAtIndex:idxLast*ntimes+itime] floatValue]/2/10;
        }
        [myvolume insertObject:[[NSNumber alloc] initWithDouble:tempvolume] atIndex:itime];
        NSLog(@"Frame: %d Volume: %g",itime,tempvolume);
        free(sliceWithRoiIdx);
    }
    return myvolume;
}

-(NSMutableArray *)getAreaArrayFor:(NSString *) RoiName{
    
    // number of slices selected
    int nslices = [viewerController maxMovieIndex];
    // number of frames in first slice
    NSMutableArray     *PixList = [viewerController pixList: 0];
    int ntimes = [PixList count];
    
    // do I have to initialize an array like this? =/
    NSNumber *temp =[[NSNumber alloc] initWithDouble:0];
    NSMutableArray *areaArray = [[NSMutableArray alloc] initWithCapacity:nslices*ntimes ];
    for (int i = 0; i < nslices * ntimes; i++)
        [areaArray insertObject:temp atIndex:i];
    
    // get all Rois with name RoiName
    for (int islice = 0; islice < [viewerController maxMovieIndex]; islice++)
    {
        // All rois contained in the current series
        NSMutableArray  *roiSeriesList  = [viewerController roiList: islice];
        for (int itime = 0; itime < ntimes; itime++)
        {
            // All rois contained in the current image
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: itime];
            for (int i = 0; i < [roiImageList count]; i++)
            {
                ROI *curROI = [roiImageList objectAtIndex: i];
                if ([[curROI name] isEqual:RoiName]){
                    NSNumber *temp = [[NSNumber alloc] initWithFloat:[curROI roiArea]];
                    [areaArray insertObject:temp atIndex:islice*ntimes+itime];
                    NSLog(@"Found %@ ROI. Slice:%d Frame:%d Area:%@",RoiName,islice,itime,[areaArray objectAtIndex:islice*ntimes+itime]);
                }
            }
        }
    }
    return areaArray;
}

/*
 math functions
 Sources:
 http://gatechgrad.wordpress.com/2011/10/08/cross-product/
 */
-(double) dotProduct:(double[3]) v1 and:(double[3]) v2 {
    return  (v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]);
}

-(void) crossProduct:(double[3]) v1 and:(double[3]) v2 result:(double[3]) vR {
    vR[0] =   ( (v1[1] * v2[2]) - (v1[2] * v2[1]) );
    vR[1] = - ( (v1[0] * v2[2]) - (v1[2] * v2[0]) );
    vR[2] =   ( (v1[0] * v2[1]) - (v1[1] * v2[0]) );
}

-(void) normalize:(double[3]) v1 result:(double[3]) vR {
    double fMag = sqrt( pow(v1[0], 2) + pow(v1[1], 2) + pow(v1[2], 2));
    vR[0] = v1[0] / fMag;
    vR[1] = v1[1] / fMag;
    vR[2] = v1[2] / fMag;
}

/*
 user interface functions
 */

-(IBAction)calcVolumes:(id)sender;
{
    NSMutableArray *volumesArrayLVEPI = [self calcVolumesNew:@"LVEPI"];
    NSMutableArray *volumesArrayLVENDO = [self calcVolumesNew:@"LVENDO"];
    NSMutableArray *volumesArrayRVEPI = [self calcVolumesNew:@"RVEPI"];
    NSMutableArray *volumesArrayRVENDO = [self calcVolumesNew:@"RVENDO"];
    // check if ES and ED makes sense
    int ntimes = [[viewerController pixList] count];
    int es = [ES intValue];
    int ed = [ED intValue];
    float bsa = [BSAfield floatValue];
    if ((es<0) || (es>ntimes) || (ed<0) || (ed>ntimes)){
        NSAlert* msgBox = [[[NSAlert alloc] init] autorelease];
        [msgBox setMessageText: @"Problem with ES or ED"];
        [msgBox addButtonWithTitle: @"OK"];
        [msgBox runModal];
        return;
    }
    double LVESV = [[volumesArrayLVENDO objectAtIndex:es-1] doubleValue];
    double LVEDV = [[volumesArrayLVENDO objectAtIndex:ed-1] doubleValue];
    double LVSV  = LVEDV-LVESV;
    double LVEF  = 100*LVSV/LVEDV;
    double LVm   = 1.05*([[volumesArrayLVEPI objectAtIndex:ed-1] doubleValue] - [[volumesArrayLVENDO objectAtIndex:ed-1] doubleValue]);
    double RVESV = [[volumesArrayRVENDO objectAtIndex:es-1] doubleValue];
    double RVEDV = [[volumesArrayRVENDO objectAtIndex:ed-1] doubleValue];
    double RVSV  = RVEDV-RVESV;
    double RVEF  = 100*RVSV/RVEDV;
    double RVm   = 1.05*([[volumesArrayRVEPI objectAtIndex:ed-1] doubleValue] - [[volumesArrayRVENDO objectAtIndex:ed-1] doubleValue]);
    
    NSString *lv = [NSString stringWithFormat:@"LV:\n%.2f\n%.2f\n%.1f\n%.2f\n%.2f\n",LVESV,LVEDV,LVSV,LVEF,LVm];
    NSString *lvbsa = [NSString stringWithFormat:@"LV/BSA:\n%.2f\n%.2f\n%.1f\n\n%.2f\n",LVESV/bsa,LVEDV/bsa,LVSV/bsa,LVm/bsa];
    NSString *rv = [NSString stringWithFormat:@"RV:\n%.2f\n%.2f\n%.1f\n%.2f\n%.2f\n",RVESV,RVEDV,RVSV,RVEF,RVm];
    NSString *rvbsa = [NSString stringWithFormat:@"RV/BSA:\n%.2f\n%.2f\n%.1f\n\n%.2f\n",RVESV/bsa,RVEDV/bsa,RVSV/bsa,RVm/bsa];
    [LVparams setStringValue:lv];
    [LVBSAparams setStringValue:lvbsa];
    [RVparams setStringValue:rv];
    [RVBSAparams setStringValue:rvbsa];
}

-(IBAction)myDrawROI:(id)sender;
{
    // TODO: change to enum with colors
    NSArray *myROINames = @[@"LVEPI",@"LVENDO",@"RVEPI",@"RVENDO"];
    RGBColor myROIColors[] = {
                             {0, 65535, 0},
                             {65535, 0, 0},
                             {65535, 65535, 0},
                             {0, 65535,65535},
    };

    // get identifier of sender button
    NSString* buttonIdentifier = [sender identifier];
    NSLog(@"Buton Identifier: %@",buttonIdentifier);
    short myROINamesIDX = [myROINames indexOfObject:buttonIdentifier];

    // get ROI list from current slice and time frame (roiImageList)
    int         curTimeFrame =  [[viewerController imageView] curImage];
    int         curSlice =  [viewerController curMovieIndex];
    NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
    
    // change ROI tool to tcpolygon
    [viewerController setROIToolTag:tCPolygon];
    
    // loop over ROIs
    for (int i = 0; i < [roiImageList count]; i++)
    {
        // find selected/drawing roi
        ROI *curROI = [roiImageList objectAtIndex: i];
        if (([curROI ROImode]==ROI_selected) || ([curROI ROImode]==ROI_selectedModify) || ([curROI ROImode]==ROI_drawing)){
            // set ROI name
            [curROI setName: [myROINames objectAtIndex:myROINamesIDX]];
            // ser ROI color
            [curROI setColor: myROIColors[myROINamesIDX]];
            // if roiMode was ROI_drawing, change to ROI_selected
            [curROI setROIMode:ROI_selected];
            // change ROI tool to tcpolygon
            [viewerController setROIToolTag:tCPolygon];
            
            NSLog(@"Selected ROI area: %g, mode: %ld",[curROI roiArea], [curROI ROImode]);
        }
    }
}

@end

